require 'rails_helper'

RSpec.describe ExecOperationsBatch do
  let(:cube1) { Matrix.create(size: 4, name: 'Matrix 4x4x4') }
  let(:cube2) { Matrix.create(size: 2, name: 'Matrix 2x2x2') }

  describe "call" do
    it "should pass HackerRank test #0" do
      batch = """
UPDATE 2 2 2 4
QUERY 1 1 1 3 3 3
UPDATE 1 1 1 23
QUERY 2 2 2 4 4 4
QUERY 1 1 1 3 3 3"""
      result = ExecOperationsBatch.call(cube1, batch)
      expect(result).to eq([nil, 4, nil, 4, 27])
    end

    it "should pass HackerRank test #1" do
      batch = """
UPDATE 2 2 2 1
QUERY 1 1 1 1 1 1
QUERY 1 1 1 2 2 2
QUERY 2 2 2 2 2 2"""
      result = ExecOperationsBatch.call(cube2, batch)
      expect(result).to eq([nil, 0, 1, 1])
    end
  end

end
