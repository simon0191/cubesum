# == Schema Information
#
# Table name: matrices
#
#  id         :integer          not null, primary key
#  size       :integer          not null
#  data       :binary           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  name       :string
#

require 'rails_helper'

RSpec.describe Matrix, type: :model do
  describe "update and query" do
    it "should pass HackerRank test #0" do
      cube = Matrix.new(size: 4, name: 'Matrix 4x4x4')
      cube.update_position(2, 2, 2, 4)
      expect(cube.sum_range(1, 1, 1, 3, 3, 3)).to eq(4)
      cube.update_position(1, 1, 1, 23)
      expect(cube.sum_range(2, 2, 2, 4, 4, 4)).to eq(4)
      expect(cube.sum_range(1, 1, 1, 3, 3, 3)).to eq(27)
    end

    it "should pass HackerRank test #1" do
      cube = Matrix.new(size: 2, name: 'Matrix 2x2x2')
      cube.update_position(2, 2, 2, 1)
      expect(cube.sum_range(1, 1, 1, 1, 1, 1)).to eq(0)
      expect(cube.sum_range(1, 1, 1, 2, 2, 2)).to eq(1)
      expect(cube.sum_range(2, 2, 2, 2, 2, 2)).to eq(1)
    end
  end

  describe "save" do
    it "should persist and load Fenwick tree" do
      cube = Matrix.create(size: 4, name: 'Matrix 4x4x4')
      cube.save
      cube = Matrix.first
      expect(cube.size).to eq(4)
      cube.update_position(2, 2, 2, 4)
      cube.save
      cube = Matrix.first
      expect(cube.sum_range(1, 1, 1, 3, 3, 3)).to eq(4)
      cube.save
      cube = Matrix.first
      cube.update_position(1, 1, 1, 23)
      cube.save
      cube = Matrix.first
      expect(cube.sum_range(2, 2, 2, 4, 4, 4)).to eq(4)
      expect(cube.sum_range(1, 1, 1, 3, 3, 3)).to eq(27)
    end
  end


end
