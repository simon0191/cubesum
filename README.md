# README

## Application layers

- Presentation:
  - app/views/**/*.html.erb
- Application/Web:
  - app/controllers/*_controller.rb: The controllers handle HTTP requests and act as intermediary between Service and Persistence layers, and Presentation layer.
- Service:
  - app/services/exec_operations_batch.rb: Parses and Processes batchs of operations.
- Persistence:
  - app/models/matrix.rb: Acts as data mapper and handles the logic that supports query and update operations.

## Refactor

### Issues

- Usage of single quotes and double quotes
- Names of variables in spanish and english
- Usage of meaningless literals like `error '2'`
- To many paths that makes difficult to read the code
- Different levels of abstraction in the same function
- Code duplication, for example `Input::get('driver_id')` is being called repeteadly instead of memoizing the result.
- Absence of DB transactions
- Absence of internationalization
- Absence of happy path (All the paths end with error)
- Domain logic inside the controller
- The controller handles all the logic

### Proposed solutions

- Use single quotes everywhere
- Use english to name all the variables
- Use Enums like `ServiceStatus::Cancelled` and `UserDeviceType::IPhone` insted of meaningless literals
- Separate the code in different classes with well defined responsibilities and same level of abstraction:
    - ConfirmServiceForm: Validates params
    - AssignDriverToServiceService: self explainatory
    - NotifyUserService: self explainatory
- Wrap Service and Driver modifications inside a DB Transaction to guarantee data integrity
- Use I18n service to send notifications to user taking into account his locale preferences.
- No more complex logic inside the controller, now, it only processes HTTP params, calls services and creates the HTTP response.
- Notify the user asynchronously because (I suppose that) the notification is not critical and it can be retried multiple times inside an asynchronous worker in case of failure.


```
<?php

  // app/controllers/service_controller.php
  class ServiceController extends ApplicationController {
    public function post_confirm() {
      $form = new ConfirmServiceForm(array(
        'service_id' => Input::get('service_id'),
        'driver_id' => Input::get('driver_id')
      ));
      if($form.is_valid()) {
        $result = AssignDriverToServiceService::call($form->service_id, $form->driver_id);
        if($result) {
          NotifyUserService::call_async($service->id);
          return Response::json(array('status' => 'success'));
        } else {
          return Response::json(array('status' => 'error', 'errors' => $result));
        }
      } else {
        return Response::json(array('status' => 'error', 'errors' => $form->errors));
      }
    }
    // app/forms/confirm_service_form.php
    class ConfirmServiceForm extends BaseForm {
      public function __construct($params) {
        $this->service_id = $params['service_id'];
        $this->driver_id = $params['driver_id'];
      }

      public function is_valid() {
        $service = Service::find(Input::get('service_id'));
        $driver = Driver::find(Input::get('driver_id'));

        if($service == NULL) {
          $this->errors['service'].push('not_found');
        }
        if($driver == NULL) {
          $this->errors['service'].push('not_found');
        }
        if($service != NULL && $service->status_id == ServiceStatus::Cancelled) {
          $this->errors['service'].push('invalid_status');
        }
        if($service != NULL && $service->driver_id != NULL || $service->status_id != ServiceStatus::PendingDriver) {
          $this->errors['service'].push('invalid_status');
        }
        if($service != NULL && $service->user->uuid == '') {
          $this->errors['user'].push('invalid_device');
        }
        return $this->errors.length != 0;
      }
    }
    // app/services/assign_driver_to_service_service.php
    class AssignDriverToServiceService extends BaseService {
      public static function call($service_id, $driver_id) {
        $service = Service::find($service_id);
        $driver = Driver::find($driver_id);
        try {
          DB::transaction(function () {
            $service = Service::update($service->id, array(
              'driver_id' => Input::get('driver_id'),
              'status_id' => ServiceStatus::Assigned,
              'car_id' => $driver->car_id
            ));
            $driver = Driver::update($driver->id, array(
              'available' => false
            ));
          });
          return true;
        } catch (DBTransactionError $e) {
          return Error::InternalServerError;
        }
      }
    }
    // app/services/notify_user_service.php
    class NotifyUserService extends BaseAsyncService {
      public static function call($service_id) {
        $service = Service::find($id);
        $push = Push::make();
        $message = I18n::translate($service->user->locale, 'service_confirmed');
        switch($service->user->type) {
          case UserDeviceType::IPhone:
            $result = $push->ios($service->user->uuid, $message, 1, 'honk.wav', 'Open', array('serviceId' => $service->id));
          break;
          case UserDeviceType::Android:
            $result = $push->android2($service->user->uuid, $message, 1, 'default', 'Open', array('serviceId' => $service->id));
          break;
          default: // Fallback
            $result = sendEmailToUser($service->user, $message);
          break;
        }
        return $result;
      }
    }
  }

?>
```
