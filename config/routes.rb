Rails.application.routes.draw do
  root to: redirect('/cubes')
  resources :cubes, only: [:index, :new, :create ] do
    member do
      get :send_batch_operations
      post :batch_operations
    end
  end
end
