class CreateMatrices < ActiveRecord::Migration[5.0]
  def change
    create_table :matrices do |t|
      t.integer :size, null: false
      t.binary :data, null: false

      t.timestamps
    end
  end
end
