class AddNameToMatrices < ActiveRecord::Migration[5.0]
  def change
    add_column :matrices, :name, :string
  end
end
