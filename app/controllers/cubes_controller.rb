class CubesController < ApplicationController

  def index
    @cubes = Matrix.all
  end

  def new
    @cube = Matrix.new
  end

  def create
    @cube = Matrix.new(cube_params)
    if @cube.save
      redirect_to cubes_path
    else
      render :new
    end
  end

  def send_batch_operations
    @cube = Matrix.find(params[:id])
  end

  def batch_operations
    @cube = Matrix.find(params[:id])
    @results = ExecOperationsBatch.call(@cube, params[:batch_operations])
  end

  private

    def cube_params
      params.require(:cube).permit(:size, :name)
    end

end