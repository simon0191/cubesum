class ExecOperationsBatch
  def self.call(cube, raw_ops)
    result = parse_raw_ops(cube, raw_ops).map(&:exec)
    cube.save
    result
  end

  private

    def self.parse_raw_ops(cube, raw_ops)
      raw_ops.split(/[\n\r]/).reject(&:blank?).map { |line| Operation.new(cube, line) }
    end

    class Operation
      OPERATION_REGEX = /(^(?<operation>UPDATE)(?<args>( \d+){4})$)|(^(?<operation>QUERY)(?<args>( \d+){6})$)/

      def initialize(cube, line)
        raise new ArgumentError('Invalid command') unless match = line.match(OPERATION_REGEX)
        @args = match[:args].split(/\s/).reject(&:blank?).map(&:to_i)
        @cube = cube
        case match[:operation]
        when 'UPDATE' then @operation = :update_position
        when 'QUERY' then @operation = :sum_range
        end
      end

      def exec
        @cube.send(@operation, *@args)
      end
    end
end