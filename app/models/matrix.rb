# == Schema Information
#
# Table name: matrices
#
#  id         :integer          not null, primary key
#  size       :integer          not null
#  data       :binary           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  name       :string
#

class Matrix < ApplicationRecord

  validates :name, presence: true
  validates :size, presence: true

  before_save :serialize_tree

  def update_position(x, y, z, val)
    curr_val = sum_range(x, y, z, x, y, z)
    delta = val - curr_val
    i = x
    while i <= self.size
      tree[i] = Hash.new(Hash.new(0)) unless tree.has_key?(i)
      j = y
      while j <= self.size
        tree[i][j] = Hash.new(0) unless tree[i].has_key?(j)
        k = z
        while k <= self.size
          tree[i][j][k] = 0 unless tree[i][j].has_key?(k)
          tree[i][j][k] += delta
          k += (k & -k)
        end
        j += (j & -j)
      end
      i += (i & -i)
    end
  end

  def read_position(x, y, z)
    sum = 0;
    i = x
    while i > 0
      j = y
      while j > 0
        k = z
        while k > 0
          sum += tree[i][j][k]
          k -= (k & -k)
        end
        j -= (j & -j)
      end
      i -= (i & -i)
    end
    sum
  end

  def sum_range(x0, y0, z0, x, y, z)
    value1 = read_position(x, y, z) - read_position(x0 - 1, y, z) -
              read_position(x, y0 - 1, z) + read_position(x0 - 1, y0 - 1, z)
    value2 = read_position(x, y, z0 - 1) - read_position(x0-1, y, z0-1) -
              read_position(x, y0-1, z0-1)  + read_position(x0 - 1, y0 - 1, z0 - 1)
    value1 - value2
  end


  private

    def tree
      @tree ||= if self.data.nil?
        Hash.new(Hash.new(Hash.new(0)))
      else
        Marshal::load(self.data)
      end
    end

    def serialize_tree
      self.data = Marshal::dump(tree)
    end

end
